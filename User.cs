﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Practice_03_06
{
    public class User
    {
        const int WITHDRAW_VALUE = 1000;
        const int ADD_VALUE = 2000;

        private int money;
        private object lockObject = new object();
        private Random random = new Random();

        public User()
        {
            money = 1000;
        }

        public void DoSomething()
        {
            lock (lockObject)
            {
                var currentThread = Thread.CurrentThread;
                Console.WriteLine($"{currentThread.ManagedThreadId} начал свою работу");
                Console.WriteLine($"У пользователя {money} денег");

                int action = random.Next(2);
                if (action == 1)
                {
                    WithdrawMoney();
                }
                else
                {
                    AddMoney();
                }
                Console.WriteLine($"У пользователя теперь {money} денег");
                Console.WriteLine($"{currentThread.ManagedThreadId} закончил свою работу");
            }
        }

        private void WithdrawMoney()
        {
            Console.WriteLine($"Снятие {WITHDRAW_VALUE}");
            money -= WITHDRAW_VALUE;
        }
        private void AddMoney()
        {
            Console.WriteLine($"Зачисление {ADD_VALUE}");
            money += ADD_VALUE;
        }
    }
}
