﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Practice_03_06
{
    class Program
    {
        static void Main(string[] args)
        {
            var threads = new Thread[20];
            var user = new User();

            DateTime timeEnd = DateTime.Now.AddMinutes(1);
            int j = 0;
            while (!MinutePassed(timeEnd))
            {
                for (int i = 0; i < threads.Length; i++)
                {
                    var thread = new Thread(user.DoSomething);
                    thread.Name = $"Поток номер: {thread.ManagedThreadId}";
                    threads[i] = thread;
                }
                foreach (var thread in threads)
                {
                    thread.Start();
                }
                Thread.Sleep(1000);
                j++;
            }
            Console.ReadLine();
        }
        
        private static bool MinutePassed(DateTime timeEnd)
        {
            if (DateTime.Now > timeEnd)
            {
                return true;
            }
            return false;
        }
    }
}
